package mx.com.hamlet.marvelapp.view.adapters

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import mx.com.hamlet.marvelapp.view.ui.CharacterFragment
import mx.com.hamlet.marvelapp.view.ui.ComicFragment

class PagerAdapter (fm : FragmentManager): FragmentStatePagerAdapter(fm) {

  //overrideMethods
  override fun getItem(position: Int): Fragment {
    when(position){
      0 -> return initCharacterView()
      1 -> return initComicView()
      else -> return initComicView()
    }
  }

  override fun getPageTitle(position: Int): CharSequence? {
    when(position){
      0 -> return "Characters"
      1 -> return "Comics"
      else -> return "Other"
    }
  }

  override fun getCount(): Int {return 2}

  //gral methods
  fun initCharacterView (): Fragment{
    val bundle = Bundle()
    return CharacterFragment()
  }

  fun initComicView (): Fragment{
    val bundle = Bundle()
    return ComicFragment()
  }

  //companion object
  companion object {
    val CHARACTER = "hero"
    val COMIC = "comic"
    val KEY_FRAGMENT = "title"
  }
}