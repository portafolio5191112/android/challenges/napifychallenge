package mx.com.hamlet.marvelapp.view.ui


import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.fragment_no_wifi.*

import mx.com.hamlet.marvelapp.R

class NoWifiFragment : DialogFragment() {

  //fun override
  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    return inflater.inflate(R.layout.fragment_no_wifi, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    closeBtn.setOnClickListener{
      fragmentManager!!.popBackStack()
      dismiss()
    }
  }

  override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
    val dialogFragment = super.onCreateDialog(savedInstanceState)
    dialogFragment.requestWindowFeature(Window.FEATURE_NO_TITLE)
    return dialogFragment
  }


}
