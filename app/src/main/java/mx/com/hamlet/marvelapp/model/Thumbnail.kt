package mx.com.hamlet.marvelapp.model

data class Thumbnail(
	val path: String? = null,
	val extension: String? = null
){
  //toString
  override fun toString(): String {
    return "Thumbnail(path=$path, extension=$extension)"
  }
}
