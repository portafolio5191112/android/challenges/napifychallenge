package mx.com.hamlet.marvelapp.view.ui


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_list.*

import mx.com.hamlet.marvelapp.R
import mx.com.hamlet.marvelapp.model.CharacterObject
import mx.com.hamlet.marvelapp.utils.App
import mx.com.hamlet.marvelapp.view.adapters.CharacterAdapter
import mx.com.hamlet.marvelapp.view.adapters.PagerAdapter
import mx.com.hamlet.marvelapp.viewmodel.MarvelApi
import mx.com.hamlet.marvelapp.viewmodel.MarvelViewModel
import retrofit2.HttpException
import java.lang.Exception

class CharacterFragment : Fragment() {

  //var
  var marvelViewModel : MarvelViewModel? = null
  lateinit var mainActivity: MainActivity
  lateinit var characterAdapter : CharacterAdapter

  //override methods
  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    mainActivity = activity as MainActivity
    marvelViewModel = mainActivity.marvelViewModel
    characterAdapter = CharacterAdapter()
    return inflater.inflate(R.layout.fragment_list, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    itemRecycler.adapter = characterAdapter
    marvelViewModel?.initCharacter(mainActivity, characterAdapter,progressBar,noConnectionContent)
    marvelViewModel!!.getCharacterList()?.observe(this, Observer {
      //Toast.makeText(mainActivity, "onDataChanged", Toast.LENGTH_SHORT).show()
      characterAdapter.setData(it)
      progressBar.visibility = View.GONE
      itemRecycler.visibility = View.VISIBLE
    })


  }

  //newInstance
  companion object {
    fun newInstance(bundle: Bundle) : CharacterFragment {
      val fragment = CharacterFragment()
      fragment.arguments = bundle
      return fragment
    }
  }
}
