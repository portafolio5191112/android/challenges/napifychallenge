package mx.com.hamlet.marvelapp.view.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_list.*

import mx.com.hamlet.marvelapp.R
import mx.com.hamlet.marvelapp.model.CharacterObject
import mx.com.hamlet.marvelapp.model.ComicObject
import mx.com.hamlet.marvelapp.view.adapters.CharacterAdapter
import mx.com.hamlet.marvelapp.view.adapters.ComicAdapter
import mx.com.hamlet.marvelapp.view.adapters.PagerAdapter
import mx.com.hamlet.marvelapp.viewmodel.MarvelApi
import mx.com.hamlet.marvelapp.viewmodel.MarvelViewModel
import retrofit2.HttpException
import java.lang.Exception

class ComicFragment : Fragment() {

  //var
  var marvelViewModel : MarvelViewModel? = null
  lateinit var mainActivity: MainActivity
  lateinit var comicAdapter: ComicAdapter

  //override fun
  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    mainActivity = activity as MainActivity
    marvelViewModel = mainActivity.marvelViewModel
    comicAdapter = ComicAdapter()
    return inflater.inflate(R.layout.fragment_list, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    itemRecycler.adapter = comicAdapter
    marvelViewModel?.initComic(mainActivity, comicAdapter,progressBar,noConnectionContent)
    marvelViewModel?.getComicList()?.observe(this, Observer {
      //Toast.makeText(mainActivity, "onDataChanged", Toast.LENGTH_SHORT).show()
      comicAdapter.setData(it)
      progressBar.visibility = View.GONE
      itemRecycler.visibility = View.VISIBLE
    })


  }

  //newInstance
  companion object {
    fun newInstance(bundle: Bundle) : ComicFragment {
      val fragment = ComicFragment()
      fragment.arguments = bundle
      return fragment
    }
  }
}
