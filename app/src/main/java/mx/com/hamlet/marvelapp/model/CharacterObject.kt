package mx.com.hamlet.marvelapp.model

import com.google.gson.annotations.SerializedName

object CharacterObject {

  data class Response(
    @SerializedName("code")
    val code: Int? = null,

    @SerializedName("data")
    val data: Data? = null,

    @SerializedName("status")
    val status: String? = null
  ){
    //toString
    override fun toString(): String {
      return "Response(code=$code, data=$data, status=$status)"
    }
  }

  //extra classes
  class Character {
    //attributes
    var name : String? = ""
    var description : String? = ""
    var thumbnail : Thumbnail? = null

    //constructor
    constructor(name: String?, description: String?, thumbnail: Thumbnail?) {
      this.name = name
      this.description = description
      this.thumbnail = thumbnail
    }

    //toString
    override fun toString(): String {
      return "Character(name=$name, " +
          "description=$description, " +
          "thumbnail=$thumbnail)"
    }

    //gral fun
    fun getURLFromImage ():String{
      return thumbnail?.path +"."+ thumbnail?.extension
    }

  }

  class Data(
    val limit: Int? = null,
    val count: Int? = null,
    val results: ArrayList<Character?>? = null
  ){
    //toString
    override fun toString(): String {
      return "Data(limit=$limit, count=$count, results=$results)"
    }
  }

}