package mx.com.hamlet.marvelapp.viewmodel

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import mx.com.hamlet.marvelapp.R
import mx.com.hamlet.marvelapp.model.CharacterObject
import mx.com.hamlet.marvelapp.model.ComicObject
import mx.com.hamlet.marvelapp.utils.App
import mx.com.hamlet.marvelapp.view.adapters.CharacterAdapter
import mx.com.hamlet.marvelapp.view.adapters.ComicAdapter
import mx.com.hamlet.marvelapp.view.ui.NoWifiFragment
import retrofit2.HttpException
import java.lang.Exception

class MarvelViewModel : ViewModel() {

  //var
  private val marvelApi by lazy {
    MarvelApi.create()
  }
  private var characterListLiveData : MutableLiveData<ArrayList<CharacterObject.Character?>>? = MutableLiveData()
  private var characterList : ArrayList<CharacterObject.Character?> = arrayListOf()
  private var comicListLiveData : MutableLiveData<ArrayList<ComicObject.Comic?>>? = MutableLiveData()
  private var comicList : ArrayList<ComicObject.Comic?> = arrayListOf()
  private var disposableCarachter: Disposable? = null
  private var disposableComic: Disposable? = null
  private var isComic : Boolean = false
  private var fragmentActivity : FragmentActivity? = null
  private var comicAdapter : ComicAdapter? = null
  private var characterAdapter : CharacterAdapter? = null
  private var progressBar : ProgressBar? = null
  private var relativeLayout : RelativeLayout? = null
  private var isConnected : Boolean? = null
  private var isConnectedLiveData : MutableLiveData<Boolean>? = null




  //constructor

  //init
  fun initComic(fragmentActivity: FragmentActivity, comicAdapter: ComicAdapter,
                progressBar: ProgressBar,relativeLayout: RelativeLayout){
    this.fragmentActivity = fragmentActivity
    this.comicAdapter = comicAdapter
    this.progressBar = progressBar
    this.relativeLayout = relativeLayout
    this.isComic = true
    this.isConnected = false
    checkConnection()
  }

  fun initCharacter(fragmentActivity: FragmentActivity, characterAdapter: CharacterAdapter,
                    progressBar: ProgressBar,relativeLayout: RelativeLayout){
    this.fragmentActivity = fragmentActivity
    this.characterAdapter = characterAdapter
    this.progressBar = progressBar
    this.relativeLayout = relativeLayout
    this.isComic = false
    this.isConnected = false
    checkConnection()
  }

  fun getHeroListToService(){
    Log.i(App.TAG, "getHeroListToService")
    try {
      disposableCarachter = marvelApi.getCharacters()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({result -> setCharactersList(result) },
          {error -> when(error){
            is HttpException -> showGenericHttpError(error)
          }
          })
    }catch (e:Exception){
      e.printStackTrace()
    }
  }

  fun getComicListToService(){
    Log.i(App.TAG, "getComicListToService")
    try {
      disposableComic = marvelApi.getComics()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({result -> setComicToList(result) },
          {error -> when(error){
            is HttpException -> showGenericHttpError(error)
          }
          })
    }catch (e:Exception){
      e.printStackTrace()
    }
  }

  //getters & setters
  fun getCharacterList() : LiveData<ArrayList<CharacterObject.Character?>>?{
    return characterListLiveData
  }

  fun getComicList() : LiveData<ArrayList<ComicObject.Comic?>>?{
    return comicListLiveData
  }

  private fun setCharactersList(marvelObject : CharacterObject.Response){
    marvelObject.data?.results?.forEach {
      addCharacter(it)
    }
  }

  fun getConnectedValue() : LiveData<Boolean>?{
    return isConnectedLiveData
  }

  fun setConnectedValue(isConnected : Boolean){
    this.isConnected = isConnected
    isConnectedLiveData?.value = this.isConnected
  }

  private fun setComicToList(comicObject: ComicObject.Response){
    comicObject.data?.results?.forEach {
      addComic(it)
    }
  }

  private fun addCharacter(character : CharacterObject.Character?){
    characterList?.add(character)
    characterListLiveData?.value = characterList
  }

  private fun addComic(comic : ComicObject.Comic?){
    comicList?.add(comic)
    comicListLiveData?.value = comicList
  }

  //gral fun
  fun checkNetworkConnection() : Boolean{
    val connectivityManager = fragmentActivity!!.getSystemService(Context.CONNECTIVITY_SERVICE)
        as ConnectivityManager
    val networkInfo : NetworkInfo? = connectivityManager.activeNetworkInfo
    return networkInfo != null && networkInfo.isConnected
  }

  fun checkWifiConnection():Boolean {
    val connectivityManager = fragmentActivity!!.getSystemService(Context.CONNECTIVITY_SERVICE)
        as ConnectivityManager
    var isWifiConnected = false
    connectivityManager.allNetworks.forEach { network ->
      connectivityManager.getNetworkInfo(network).apply {
        if (type == ConnectivityManager.TYPE_WIFI) {
          isWifiConnected = isConnected or isWifiConnected
        }
      }
    }
    return isWifiConnected
  }

  fun checkMobileConnection():Boolean {
    val connectivityManager = fragmentActivity!!.getSystemService(Context.CONNECTIVITY_SERVICE)
        as ConnectivityManager
    var isMobileConnected = false
    connectivityManager.allNetworks.forEach { network ->
      connectivityManager.getNetworkInfo(network).apply {
        if (type == ConnectivityManager.TYPE_MOBILE) {
          isMobileConnected = isConnected or isMobileConnected
        }
      }
    }
    return isMobileConnected
  }

  fun checkConnection(){
    setConnectedValue(checkNetworkConnection())
    if (checkNetworkConnection()){
      if (isComic){
        getComicListToService()
      }else{
        getHeroListToService()
      }
    }else{
      showDialogWifi()
    }
  }

  fun showGenericHttpError(error: Throwable) {
    val  httpException = error as HttpException
    if(httpException.code() == 409) {

    }
  }

  fun showDialogWifi(){
    progressBar?.visibility = View.GONE
    relativeLayout?.visibility = View.VISIBLE
    val dialogFragment = NoWifiFragment()
    val fragmentManager : FragmentManager = fragmentActivity!!.supportFragmentManager
    val transaction: FragmentTransaction = fragmentManager.beginTransaction()
    transaction.add(android.R.id.content, dialogFragment)
      .addToBackStack(null).commit()
  }

  //fun probes
  companion object {

    fun heroList(context: Context?) : ArrayList<CharacterObject.Character>{
      var heroList : ArrayList<CharacterObject.Character> = arrayListOf()
      heroList.add(CharacterObject.Character("hulk", context?.getString(R.string.loremIpsium), null))
      heroList.add(CharacterObject.Character("capitan america", context?.getString(R.string.loremIpsium), null))
      heroList.add(CharacterObject.Character("spiderman", context?.getString(R.string.loremIpsium), null))
      heroList.add(CharacterObject.Character("deadpool", context?.getString(R.string.loremIpsium), null))
      return heroList
    }

    fun comicList(context: Context?): ArrayList<ComicObject.Comic>{
      val comicList : ArrayList<ComicObject.Comic> = arrayListOf()
      comicList.add(ComicObject.Comic("the amazing spiderman", context?.getString(R.string.loremIpsium), null))
      comicList.add(ComicObject.Comic("x-force", context?.getString(R.string.loremIpsium), null))
      comicList.add(ComicObject.Comic("deadpool", context?.getString(R.string.loremIpsium), null))
      comicList.add(ComicObject.Comic("univers hulk", context?.getString(R.string.loremIpsium), null))
      return comicList
    }

  }

}