package mx.com.hamlet.marvelapp.viewmodel

import io.reactivex.Observable
import mx.com.hamlet.marvelapp.model.CharacterObject
import mx.com.hamlet.marvelapp.model.ComicObject
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit

interface MarvelApi {

  //request
  @GET("characters?limit=100&apikey=9ca9f2d9adbf4c035ec1df2e4ad8400c&ts=9&hash=b487dba832cb7b782e4f5d2de3d08c56")
  fun getCharacters() : Observable<CharacterObject.Response>
  @GET("comics?limit=100&apikey=9ca9f2d9adbf4c035ec1df2e4ad8400c&ts=9&hash=b487dba832cb7b782e4f5d2de3d08c56")
  fun getComics() : Observable<ComicObject.Response>

  //companion object
  companion object {
    fun create() : MarvelApi{
      val client = OkHttpClient.Builder()
        .readTimeout(2,TimeUnit.MINUTES)
        .writeTimeout(2, TimeUnit.MINUTES)
        .connectTimeout(2, TimeUnit.MINUTES)
        .build()

      val retrofit = Retrofit.Builder()
        .client(client)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl("https://gateway.marvel.com/v1/public/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

      return retrofit.create(MarvelApi::class.java)
    }
  }
}