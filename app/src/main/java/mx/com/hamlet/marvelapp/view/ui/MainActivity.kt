package mx.com.hamlet.marvelapp.view.ui


import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_main.*
import mx.com.hamlet.marvelapp.R
import mx.com.hamlet.marvelapp.view.adapters.PagerAdapter
import mx.com.hamlet.marvelapp.viewmodel.MarvelViewModel

class MainActivity : FragmentActivity() {

  //var
  var marvelViewModel : MarvelViewModel?  = null

  //fun override
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    //initTabs & viewPager
    viewPager.adapter = PagerAdapter(supportFragmentManager)
    tabsMenu.setupWithViewPager(viewPager)
    initIconTbas()
    marvelViewModel = ViewModelProviders.of(this).get(MarvelViewModel::class.java)
  }

  private fun initIconTbas(){
    tabsMenu.getTabAt(0)?.setIcon(R.drawable.ic_character_color)
    tabsMenu.getTabAt(1)?.setIcon(R.drawable.ic_comic_color)
  }

}
