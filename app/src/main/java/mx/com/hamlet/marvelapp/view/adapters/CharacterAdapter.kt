package mx.com.hamlet.marvelapp.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_card.view.*
import mx.com.hamlet.marvelapp.R
import mx.com.hamlet.marvelapp.model.CharacterObject

class CharacterAdapter : RecyclerView.Adapter<CharacterAdapter.ViewHolder>() {

  //var
  var heroList : ArrayList<CharacterObject.Character?>? = arrayListOf()


  //override methods
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_card, parent, false))
  }

  override fun getItemCount(): Int {
    return heroList!!.size
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.bindview(heroList!!.get(position)!!)
  }

  fun setData(newHeroList : ArrayList<CharacterObject.Character?>?){
    if (newHeroList != null){
      var heroDiffCallback = CharacterDiffCallback(heroList, newHeroList)
      var diffUtilResult = DiffUtil.calculateDiff(heroDiffCallback)
      heroList!!.clear()
      heroList!!.addAll(newHeroList)
      diffUtilResult.dispatchUpdatesTo(this)
    }else{
      heroList = newHeroList
    }
    notifyDataSetChanged()
  }

  //extra class
  class CharacterDiffCallback : DiffUtil.Callback {

    //var
    var oldUserList : ArrayList<CharacterObject.Character?>? = arrayListOf()
    var newUserList : ArrayList<CharacterObject.Character?>? = arrayListOf()

    constructor(
      oldUserList: ArrayList<CharacterObject.Character?>?,
      newUserList: ArrayList<CharacterObject.Character?>?
    ) : super() {
      this.oldUserList = oldUserList
      this.newUserList = newUserList
    }


    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
      return oldUserList!!.get(oldItemPosition)!!.name== newUserList!!.get(newItemPosition)!!.name
    }

    override fun getOldListSize(): Int {
      return oldUserList!!.size
    }

    override fun getNewListSize(): Int {
      return newUserList!!.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
      return oldUserList!!.get(oldItemPosition) == newUserList!!.get(newItemPosition)
    }

  }

  //viewHolder
  class ViewHolder : RecyclerView.ViewHolder{

    //contructor
    constructor(itemView: View) : super(itemView)

    //bindView
    fun bindview(hero : CharacterObject.Character){
      itemView.title.text = hero.name
      if(!hero.description.isNullOrEmpty()){
        itemView.description.text = hero.description
      }else{
        itemView.description.text = "No description"
      }
      Glide.with(itemView)
        .load(hero.getURLFromImage())
        .into(itemView.thumbnail)
    }
  }
}