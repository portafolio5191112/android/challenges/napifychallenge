package mx.com.hamlet.marvelapp.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_card.view.*
import mx.com.hamlet.marvelapp.R
import mx.com.hamlet.marvelapp.model.ComicObject

class ComicAdapter : RecyclerView.Adapter<ComicAdapter.ViewHolder>() {

  //var
  var comicList : ArrayList<ComicObject.Comic?>? = arrayListOf()

  //override methods
  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_card,parent,false))
  }

  override fun getItemCount(): Int {
    return comicList!!.size
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.bindView(comicList!!.get(position)!!)
  }

  fun setData(newComicList : ArrayList<ComicObject.Comic?>?){
    if (newComicList != null){
      var heroDiffCallback = CharacterDiffCallback(comicList, newComicList)
      var diffUtilResult = DiffUtil.calculateDiff(heroDiffCallback)
      comicList!!.clear()
      comicList!!.addAll(newComicList)
      diffUtilResult.dispatchUpdatesTo(this)
    }else{
      comicList = newComicList
    }
    notifyDataSetChanged()
  }

  //extra class
  class CharacterDiffCallback : DiffUtil.Callback {

    //var
    var oldUserList : ArrayList<ComicObject.Comic?>? = arrayListOf()
    var newUserList : ArrayList<ComicObject.Comic?>? = arrayListOf()

    constructor(
      oldUserList: ArrayList<ComicObject.Comic?>?,
      newUserList: ArrayList<ComicObject.Comic?>?
    ) : super() {
      this.oldUserList = oldUserList
      this.newUserList = newUserList
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
      return oldUserList!!.get(oldItemPosition)!!.title== newUserList!!.get(newItemPosition)!!.title
    }

    override fun getOldListSize(): Int {
      return oldUserList!!.size
    }

    override fun getNewListSize(): Int {
      return newUserList!!.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
      return oldUserList!!.get(oldItemPosition) == newUserList!!.get(newItemPosition)
    }

  }

  //viewHolder
  class ViewHolder : RecyclerView.ViewHolder{

    //constructor
    constructor(itemView: View) : super(itemView)

    //bindView
    fun bindView(comic : ComicObject.Comic){
      itemView.title.text = comic.title
      if (!comic.description.isNullOrEmpty()){
        itemView.description.text = comic.description
      }else{
        itemView.description.text = "No description"
      }

      Glide.with(itemView)
        .load(comic.getURLFromImage())
        .into(itemView.thumbnail)
    }
  }
}